terraform {
  backend "s3" {
    bucket = "base-config-344754"
    key    = "gitlab-runner-fleet"
    region = "us-east-1"
  }
}
